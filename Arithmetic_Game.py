import random
import datetime

POINTS_TO_WIN = 10
SCORES_FILE = 'Highscores'

class Player:
    def __init__(self, name, errors, time):
        self.name = name
        self.errors = errors
        self.time = time

    def setName(self, name):
        self.name = name
        return self.name

    def setNumberOfErrors(self, num):
        self.errors = num
        return self.errors

    def setTimeElapsed(self, time):
        self.time = time
        return self.time

    def getName(self):
        return self.name

    def getNumberOfErrors(self):
        return self.errors

    def getTime(self):
        return self.time

    def updateStats(self, newErrors, newTime):
        if (newTime < self.time or (newTime == self.Time and newErrors < self.errors)):
            self.time = newTime
            self.errors = newErrors
            return True
        else:
            return False

class Questions:
    def generateQuestion(self):
        questions = [self.additionProblem, self.subtractionProblem, self.multiplicationProblem, self.divisionProblem]
        return random.choice(questions)()

    def additionProblem(self):
        x = random.randrange(10,100)
        y = random.randrange(10, 100)
        return [1, x,y,x+y]

    def subtractionProblem(self):
        x = random.randrange(10,100)
        y = random.randrange(10, x)
        return [2, x, y, x - y]

    def multiplicationProblem(self):
        x = random.randrange(10, 100)
        y = random.randrange(2, 10)
        return [3, x, y, x * y]

    def divisionProblem(self):
        y = random.randrange(2, 10)
        x = random.choice([x for x in range(100, 1000) if x % y == 0])
        return [4, x, y, int(x / y)]

def readFile(filename):
    try:
        with open(filename, 'r+') as f:
            scores = [word.strip().split(',') for word in f]
        Players = []
        for score in scores:
            Players.append(Player(score[0], score[1].strip(), score[2].strip()))
        return Players
    finally:
        f.close()

def updateFile(players, filename):
    try:
        toWrite = []
        for player in players:
            toWrite.append("%s,%s,%s\n" % (player.getName(), player.getNumberOfErrors(), player.getTime()))
        with open(filename, 'w+') as f:
            f.writelines(toWrite)
    finally:
        f.close()

def showHighScore(players):
    ret = "\n     =========================\n     == CURRENT LEADERBOARD ==\n     =========================\n\n"

    scores = []
    for player in players:
        scores.append([player.getName(), player.getNumberOfErrors(), player.getTime()])

    scores = sorted(scores, key=lambda x: (x[2], x[1]))
    line = 0
    for names in scores:
        line += 1
        if line == 11:
            break
        ret += "%d. %s \n" % (line, names[0])
        ret += "   � %s errors in %d min and %d sec\n" % (names[1], int(names[2])/60, int(names[2])%60)
    ret += "\nThank you for playing the Arithmetic Game!"
    return ret

def gameplay(pointsToWin):
    uname = input("Please enter your username: ")
    input("Press ENTER if your are ready...")

    score = 0
    errors = 0
    timeConsumed = datetime.datetime.now()

    while score != pointsToWin and errors != pointsToWin:
        NewQuestion = Questions()
        question = NewQuestion.generateQuestion()
        if question[0] == 1:
            answer = input("\nWhat is %d + %d? " % (question[1], question[2]))
        elif question[0] == 2:
            answer = input("\nWhat is %d - %d? " % (question[1], question[2]))
        elif question[0] == 3:
            answer = input("\nWhat is %d * %d? " % (question[1], question[2]))
        else:
            answer = input("\nWhat is %d / %d? " % (question[1], question[2]))

        if int(answer) == int(question[3]):
            score += 1
            print("     Congratulations! Try another one.")
            print("     Score/Errors: %d/%d" % (score, errors))
        else:
            errors += 1
            print("     The answer should be %d. Keep practicing." % question[3])
            print("     Score/Errors: %d/%d" % (score, errors))

    return [datetime.datetime.now() - timeConsumed, score, errors, uname]

def SaveScore(players, uname, errors, time):
    index = [x for x in range(len(players)) if uname in players[x].getName()]

    if len(index) > 0:
        if time.seconds < int(players[index[0]].getTime()) or (time.seconds == int(players[index[0]].getTime()) and errors < int(players[index[0]].getNumberOfErrors())):
            players[index[0]].setTimeElapsed(str(time.seconds))
            players[index[0]].setNumberOfErrors(str(errors))
            updateFile(players, SCORES_FILE)
            return "Your new score is %d errors in %d min and %d sec." % (int(players[index[0]].getNumberOfErrors()), int(players[index[0]].getTime()) / 60, int(players[index[0]].getTime())%60)
        else:
            return "You did not beat your previous score of %d errors in %d min and %d sec." % (int(players[index[0]].getNumberOfErrors()), int(players[index[0]].getTime()) / 60, int(players[index[0]].getTime())%60)
    else:
        players.append(Player(uname, str(errors), str(time.seconds)))
        updateFile(players, SCORES_FILE)


def main():
    players = readFile(SCORES_FILE)
    prompt = "Y"

    print("\n== Welcome to the Arithmetic Game! ==")
    print(showHighScore(players))

    while prompt == "Y":
        result = gameplay(POINTS_TO_WIN)
        if result[1] == POINTS_TO_WIN:
            print()
            print("Congratulations! You have finished the game in %d min and %d sec" % (
            result[0].seconds / 60, result[0].seconds % 60))
            SaveScore(players, result[3], result[2], result[0])
            prompt = input("\nWould you like to try again? (Y|N) ").upper()
            if prompt != "Y":
                print(showHighScore(players))
        else:
            prompt = input("You have committed 10 mistakes. Would you like to try again? (Y|N) ").upper()
            if prompt != "Y":
                print(showHighScore(players))

main()